int width = screen.width;
int height = screen.height;

int nTentacles = 4;

int nSegs = 30;
float totalLength = width/2;
float downScaling = .9;
float baseLength;
float baseThickness = 10;
float maxdTheta = 1.7*(2*PI)/nSegs;//max angle by which it can differ from its predecessor
float maxdThetaVelPerStep = maxdTheta/30;//max velocity it can change by each step
float maxThetaVel = maxdThetaVelPerStep*2;
float trickleVelScaling = nSegs*.005;

boolean centeredTentacles = false;
boolean drawSuckers = true;

color backgroundColor = color(0,0,0);
color tentacleColor = 0;
color suckerColor = 255;
color suckerOutlineColor = 0;
float suckerScale = .8;


Tentacle[] tentacles;


void setup(){
  frameRate(30);
  smooth();
  size(screen.width,2*screen.height);
  tentacles = new Tentacle[nTentacles];
  randomizeColors();
  for(int i=0; i<nTentacles; i++){
    if(centeredTentacles)
      tentacles[i] = new Tentacle(width/2, height/2, TWO_PI/nTentacles*i-PI/2);
    else
//      tentacles[i] = new Tentacle(random(width), random(height), random(TWO_PI));
//      tentacles[i] = new Tentacle(random(width), totalLength/2+random(height-totalLength/2), -PI/2);
      //tentacles[i] = new Tentacle(width/2+totalLength/1.5*cos(TWO_PI/nTentacles*i), 
                                  //height/2+totalLength/1.5*sin(TWO_PI/nTentacles*i),
                                  //TWO_PI/nTentacles*i+PI);
      if(i%2==0){
        tentacles[i]= new Tentacle(width,height/2 + i * height/nTentacles, 3 * PI/4);

      }else{

        tentacles[i]= new Tentacle(0,height/2 + i * height/nTentacles,   PI/4);
      }
  }
}

void draw(){
  blankScreen();
  for(int i=0; i<nTentacles; i++){
    tentacles[i].drawTentacle();
    tentacles[i].wiggleTentacle();
  }
}

void blankScreen(){
  fill(backgroundColor);
  noStroke();
  //rect(0,0,width,height);
  background(backgroundColor);
}
