import processing.core.*; 
import processing.xml.*; 

import java.applet.*; 
import java.awt.*; 
import java.awt.image.*; 
import java.awt.event.*; 
import java.io.*; 
import java.net.*; 
import java.text.*; 
import java.util.*; 
import java.util.zip.*; 
import java.util.regex.*; 

public class tentacle extends PApplet {

class Tentacle{
  float x0,y0,theta0;
  float[] segLengths;
  float[] segThetas;
  
  Tentacle(float x0i, float y0i, float theta0i){
    x0 = x0i;
    y0 = y0i;
    theta0 = theta0i;
  
    baseLength = totalLength*(downScaling-1)/(pow(downScaling,nSegs)-1);
    
    segLengths = new float[nSegs];
    segThetas = new float[nSegs];
    for(int i=0; i<nSegs; i++){
      if(i==0)
        segLengths[i] = baseLength;
      else
        segLengths[i] = segLengths[i-1]*downScaling;
    }
  }

  public void drawTentacle(){
    float x1 = x0;
    float y1 = y0;
    float x2 = x0;
    float y2 = y0;
    float totalTheta=theta0;
    for(int i=0; i<nSegs; i++){
      stroke(tentacleColor);
      strokeWeight(baseThickness*segLengths[i]/segLengths[0]);
      totalTheta+=segThetas[i];
      x2 = x1+segLengths[i]*cos(totalTheta);
      y2 = y1+segLengths[i]*sin(totalTheta);
      line(x1,y1,x2,y2);
      if(drawSuckers){
        fill(suckerColor);
        stroke(suckerOutlineColor);
        strokeWeight(1);
        ellipse((x1+x2)/2,(y1+y2)/2,
                baseThickness*segLengths[i]/segLengths[0]*suckerScale,
                baseThickness*segLengths[i]/segLengths[0]*suckerScale);

      }
      x1 = x2;
      y1 = y2;
    }
  }
  
  
  float[] segVelocities = new float[nSegs];
  public void wiggleTentacle(){
    for(int i = 0; i<nSegs; i++){
      //check for boundaries
      if(segThetas[i] <= -maxdTheta || segThetas[i] >= maxdTheta)
        segVelocities[i] = 0;
    }
    //trickle-down velocity
    for(int i = nSegs-2; i>=0; i--){
      segVelocities[i] += segVelocities[i+1]*trickleVelScaling; 
    }
    
    //change tip velocity a bit!
    segVelocities[nSegs-1] += random(-maxdThetaVelPerStep,maxdThetaVelPerStep);
    
    //limit velocities, eliminating sudden stops
    int sign = 1;
    for(int i = 0; i<nSegs; i++){
      if(segThetas[i]>0 && segVelocities[i]>0){
        segVelocities[i] = min(segVelocities[i],
                               maxThetaVel*(1-segThetas[i]/maxdTheta));
      }
    if(segThetas[i]<0 && segVelocities[i]<0){
        segVelocities[i] = min(abs(segVelocities[i]),
                               maxThetaVel*(1-abs(segThetas[i])/maxdTheta));
        segVelocities[i] = -segVelocities[i];
      }
    }
    
    //move everyone
    for(int i = 0; i<nSegs; i++){
      segThetas[i] = constrain(segThetas[i]+segVelocities[i],
                               -maxdTheta*(1-i/nSegs),
                               maxdTheta*(1-i/nSegs));
    }
  }
}
public void keyTyped(){
  if(key ==' '){
    setup();
  }
  if(key =='r'){
    maxdTheta = 1.5f*(2*PI)/nSegs;//max angle by which it can differ from its predecessor
    maxdThetaVelPerStep = maxdTheta/40;//max velocity it can change by each step
    maxThetaVel = maxdThetaVelPerStep*2;
    trickleVelScaling = nSegs*.003f;
    
    centeredTentacles = true;
    drawSuckers = true;
    
    backgroundColor = 255;
    tentacleColor = 0;
    suckerColor = 255;
    suckerOutlineColor = 0;
    setup();
  }
  if(key=='s')
    drawSuckers = !drawSuckers;
  if(key=='c'){
    backgroundColor = color(random(255),random(255),random(255));
    tentacleColor = color(random(255),random(255),random(255));
    suckerColor = color(random(255),random(255),random(255));
    suckerOutlineColor = color(random(255),random(255),random(255));
  }  
  
  if(key =='l'){
    centeredTentacles = !centeredTentacles;
    setup();
  }
  if(key=='-'){
    maxThetaVel = maxThetaVel/1.2f;
    println("thetavel: "+(maxThetaVel/(maxdThetaVelPerStep*2)));
  }
  if(key=='_'){
    maxThetaVel = maxThetaVel*1.2f;
    println("thetavel: "+(maxThetaVel/(maxdThetaVelPerStep*2)));
  }
  
  if(key=='='){
    trickleVelScaling = trickleVelScaling/1.2f;
    println("trickleVel: "+(trickleVelScaling/(nSegs*.003f)));
  }
  if(key=='+'){
    trickleVelScaling = trickleVelScaling*1.2f;
    println("trickleVel: "+(trickleVelScaling/(nSegs*.003f)));
  }

  if(key=='['){
    maxdTheta = maxdTheta/1.2f;
    println("maxdTheta: "+(maxdTheta/(1.5f*(2*PI)/nSegs)));
  }
  if(key=='{'){
    maxdTheta = maxdTheta*1.2f;
    println("maxdTheta: "+(maxdTheta/(1.5f*(2*PI)/nSegs)));
  }
  
  if(key==']'){
    nTentacles = constrain(nTentacles-1, 1, 2000);
    setup();
  }
  if(key=='}'){
    nTentacles = constrain(nTentacles+1, 1, 2000);
    setup();
  }
}
int width = 500;
int height = 500;

int nTentacles = 5;

int nSegs = 30;
float totalLength = width/2;
float downScaling = .9f;
float baseLength;
float baseThickness = 10;
float maxdTheta = 1.5f*(2*PI)/nSegs;//max angle by which it can differ from its predecessor
float maxdThetaVelPerStep = maxdTheta/40;//max velocity it can change by each step
float maxThetaVel = maxdThetaVelPerStep*2;
float trickleVelScaling = nSegs*.003f;

boolean centeredTentacles = true;
boolean drawSuckers = true;

int backgroundColor = color(255,255,255);
int tentacleColor = 0;
int suckerColor = 255;
int suckerOutlineColor = 0;
float suckerScale = .8f;


Tentacle[] tentacles;


public void setup(){
  frameRate(30);
  smooth();
  size(width,height);
  tentacles = new Tentacle[nTentacles];
  for(int i=0; i<nTentacles; i++){
    if(centeredTentacles)
      tentacles[i] = new Tentacle(width/2, height/2, TWO_PI/nTentacles*i-PI/2);
    else
//      tentacles[i] = new Tentacle(random(width), random(height), random(TWO_PI));
//      tentacles[i] = new Tentacle(random(width), totalLength/2+random(height-totalLength/2), -PI/2);
      tentacles[i] = new Tentacle(width/2+totalLength/1.5f*cos(TWO_PI/nTentacles*i), 
                                  height/2+totalLength/1.5f*sin(TWO_PI/nTentacles*i),
                                  TWO_PI/nTentacles*i+PI);
  }
}

public void draw(){
  blankScreen();
  for(int i=0; i<nTentacles; i++){
    tentacles[i].drawTentacle();
    tentacles[i].wiggleTentacle();
  }
}

public void blankScreen(){
  fill(backgroundColor);
  noStroke();
  rect(0,0,width,height);
}

  static public void main(String args[]) {
    PApplet.main(new String[] { "--bgcolor=#F0F0F0", "tentacle" });
  }
}
