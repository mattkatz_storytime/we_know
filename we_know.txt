We know what you've been breeding 
in the sewers, Mr Fisk.
The egg sacs blocked the runoff
so we're torching all of this

You've violated bylaws 
and tempted all the fates
The next hard rain would flood us
and pop the drainage grates

We've had our eye on you 
and your creepy little plan
parasitic mind wasps are 
against the laws of god and man

We've got no zoning guidance
For these artificial moons
But the revenue service inquires
Are you witholding tax for all your goons?

We know you haven't filed
with the city, Mr Fisk.
No one approved the tunnels
Or inspected all of this.

We've found the creeping goo
It's nearly eaten through the cage
Poor Bob poked the surface
How do we make it disengage?

We've had our eye on you
your un-permitted lasers
Any major construction means
You've got to file papers

Careful mind your head, sir
See how we treat you soft?
Your booby traps are clever
Now help us shut them off.

We know what you've been building 
In the caves, Mr Fisk.
Your experiment's at an end 
we're impounding all of this.

This Heskly-Maddox Brainbox
has pathways quite illegal
The regulator's filed down
and the impeller set to evil

We've had our eye on you 
and your positronic purchases
No accountant has a cause
to buy folding klein-space surfaces

The lab folks are arriving
with their photos, tape, and dust
But you won't get the pleasure
You'll have to come with us
